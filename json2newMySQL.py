import json
import app.models as newModel
from django.db.utils import IntegrityError

newModelList = [
    'Topic', 'Answer', 'Element', 'ElementMedia', 'Media', 'MediaTyp', 'Question', 'QuestionAnswer', 'Structure'
]

# empty all tables of new db
# for m in newModelList:
#     getattr(newModel, m).objects.all().delete()

mediaTypeDict = {
    5: 'Radiologie',
    0: 'Foto',
    4: 'Schnitt'
}

qType = {
    0: False,
    1: True
}

qRank = {
    0: True,
    1: False
}

# in the export tool Neuroanatomie is called neurologische Anatomie which is no good.
renameTopic = {
    'bewegung': 'Bewegungsapparat',
    'topo': 'Topograpische Anatomie',
    'neuro': 'Neuroanatomie',

}

infile = open("db_dumps/tuanaDB.json", "r")
eList = json.load(infile)

for e in eList:
    print(e['name_med'])
    topicObj = {}
    try:
        topicObj = newModel.Topic.objects.create(name=renameTopic[e['topic']['nick']], nick=e['topic']['nick'])
    except IntegrityError:
        topicObj = newModel.Topic.objects.get(nick=e['topic']['nick'])

    eObj = {}
    try:
        eObj = newModel.Element.objects.create(
            name_alt=None if not e['name_alt'] else e['name_alt'].strip(),
            name_med=None if not e['name_med'] else e['name_med'].strip(),
        )
    except IntegrityError:
        eObj = newModel.Element.objects.get(
            name_med=None if not e['name_med'] else e['name_med'].strip(),
        )
    if topicObj in eObj.topics.all():
        continue
    else:
        eObj.topics.add(topicObj)


    for s in e['structures']:
        structObj = newModel.Structure.objects.get_or_create(name=s['name'])
        eObj.structures.add(structObj[0])


    elMedL = []
    for elMed in e['elementMedia']:
        mediaTyp = {}
        try:
            mediaTyp = newModel.MediaTyp.objects.create(name=mediaTypeDict[elMed['media']['typ']])
        except IntegrityError:
            mediaTyp = newModel.MediaTyp.objects.get(name=mediaTypeDict[elMed['media']['typ']])

        med = {}
        try:
            med = newModel.Media.objects.create(
                typ=mediaTyp,
                url=elMed['media']['url'],
                name=None if not elMed['media']['name'] else elMed['media']['name'].strip(),
                beschreibung=None if not elMed['media']['beschreibung'] else elMed['media']['beschreibung'].strip()
            )
        except IntegrityError:
            med = newModel.Media.objects.get(
                url=elMed['media']['url'],
            )

        try:
            newModel.ElementMedia.objects.create(
                position=elMed['position'],
                position_text=elMed['position_text'],
                no=elMed['no'],
                is_overview=elMed['is_overview'],
                media=med,
                element=eObj
            )
        except IntegrityError:
            continue

    for q in e['questions']:
        newQ = {}
        try:
            newQ = newModel.Question.objects.create(
                correct_answers_needed=q['correct_answers_needed'],
                element=eObj,
                is_mc=qType[q['typ']],
                is_primary=qRank[q['sequenz']],
                text=None if not q['text'] else q['text'].strip(),
                topic=topicObj,
                modus=q['modus'],
                published=q['published'],
            )
        except IntegrityError:
            newQ = newModel.Question.objects.get(
                correct_answers_needed=q['correct_answers_needed'],
                element=eObj,
                is_mc=qType[q['typ']],
                is_primary=qRank[q['sequenz']],
                text=None if not q['text'] else q['text'].strip(),
                topic=topicObj,
            )

        for a in q['answers']:
            aText = None
            eObjToAdd = eObj
            if a['text'] and len(a['text']) > 0:
                aText = None if not a['text'] else a['text'].strip()
                eObjToAdd = None

            try:
                newA = newModel.Answer.objects.create(
                    text=aText,
                    element=eObjToAdd
                )
            except IntegrityError:
                newA = newModel.Answer.objects.get(
                    text=aText,
                    element=eObjToAdd
                )

            try:
                newModel.QuestionAnswer.objects.create(
                    question=newQ,
                    answer=newA,
                    correct=a['correct']
                    )
            except IntegrityError:
                continue

print('\n\n\n\nAnswer Schizzle')
for a in newModel.Answer.objects.all():
    print(a)
    try:
        elem = newModel.Element.objects.get(name_med=a.text)
    except:
        continue
    else:
        a.element = elem
        a.text = None
        a.save()
