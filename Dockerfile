ARG PYTHON_VERSION=3.10-slim-bullseye

# define an alias for the specfic python version used in this file.
FROM python:${PYTHON_VERSION} as python

# Python build stage
FROM python as python-build-stage

EXPOSE 8000

ARG DEPLOY_ENV
ARG APP_HOME=/app/project

# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
  build-essential \
  # psycopg2 dependencies
  libpq-dev \
  # git for installing from custom git repos
  git \
  # curl for downloading files
  curl tar gcc libopenjp2-7 libopenjp2-tools

# prepare log folder
# Create source code environment
RUN mkdir -p /opt/src
WORKDIR /opt/src

RUN mkdir -p /opt/media
RUN chmod -R a+w /opt/media

ADD . ./
WORKDIR /opt/src/project


# install django app
RUN pip install --upgrade pip
RUN pip install -r ../requirements/production.txt

#


CMD bash -c "python manage.py runserver 0.0.0.0:8000 --settings=config.settings.production"
