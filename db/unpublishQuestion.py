import csv
from app.models import Answer, Element, Question
notFound = open('db/notFound.csv', 'w')
found = open('db/found.csv', 'w')
writerNf = csv.writer(notFound)
writerF = csv.writer(found)

with open('db/unpublish.csv', 'r', newline='') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        try:
            q = Question.objects.get(id=row[0])
        except Exception:
            print(sys.exc_info())
            writerNf.writerow(row)
        else:
            writerF.writerow(row)
            q.published = True
            q.save()

with open('db/unpublish2.csv', 'r', newline='') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        try:
            q = Question.objects.get(id=row[0])
        except Exception:
            print(sys.exc_info())
            writerNf.writerow(row)
        else:
            writerF.writerow(row)
            q.published = False
            q.save()

notFound.close()
found.close()
