#!/bin/bash
CONF_TOPO=./anatomie_topo.cnf
CONF_NEURO=./anatomie_neuro.cnf
CONF_BEWEGUNG=./anatomie_bewegung.cnf
CONF_NEW=./anatomie_new.cnf

mysqldump --defaults-extra-file=$CONF_TOPO -u tutorial_anatomie_unibas_ch_01 -h its-web-sql-003.its.unibas.ch tutorial_anatomie_unibas_ch_01 > anatomie_topo.sql
mysqldump --defaults-extra-file=$CONF_NEURO -u tutorial_anatomie_unibas_ch_02 -h its-web-sql-003.its.unibas.ch tutorial_anatomie_unibas_ch_02 > anatomie_neuro.sql
mysqldump --defaults-extra-file=$CONF_BEWEGUNG -u tutorial_anatomie_unibas_ch_03 -h its-web-sql-003.its.unibas.ch tutorial_anatomie_unibas_ch_03 > anatomie_bewegung.sql


counter=0;for f in `cat replaceTopo.txt`;do replace[counter]=$f;counter=$counter+1;done;counter2=0;
for ((a=0; a <= $counter-1; a=a+2)); do `sed -i "" "s/${replace[$a]}/${replace[$a+1]}/g" anatomie_topo.sql` ;done;

counter=0;for f in `cat replaceNeuro.txt`;do replace[counter]=$f;counter=$counter+1;done;counter2=0;
for ((a=0; a <= $counter-1; a=a+2)); do `sed -i "" "s/${replace[$a]}/${replace[$a+1]}/g" anatomie_neuro.sql` ;done;

counter=0;for f in `cat replaceBewegungsapparat.txt`;do replace[counter]=$f;counter=$counter+1;done;counter2=0;
for ((a=0; a <= $counter-1; a=a+2)); do `sed -i "" "s/${replace[$a]}/${replace[$a+1]}/g" anatomie_bewegung.sql` ;done;

mysql --defaults-extra-file=$CONF_NEW -h its-web-sql-009.its.unibas.ch -u test_nmc_01 test_nmc_01 < anatomie_topo.sql
mysql --defaults-extra-file=$CONF_NEW -h its-web-sql-009.its.unibas.ch -u test_nmc_01 test_nmc_01 < anatomie_neuro.sql
mysql --defaults-extra-file=$CONF_NEW -h its-web-sql-009.its.unibas.ch -u test_nmc_01 test_nmc_01 < anatomie_bewegung.sql
