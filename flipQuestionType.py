import json
from django.db import IntegrityError
from app.models import Question
from app import serializers

allQ = Question.objects.all()
integrityErrorQ = []

for q in allQ:
    q.is_mc = not q.is_mc
    try:
        q.save()
    except IntegrityError:
        q.text = "{}wxyz".format(q.text)
        q.save()
        qJson = json.dumps(serializers.question(q))
        integrityErrorQ.append(qJson)

with open('IntegrityErrorQ.json', 'w') as openfile:
    json.dump(integrityErrorQ, openfile, indent=2)

for q in Question.objects.filter(text__contains="wxyz"):
    q.text = q.text[0:-4]
    q.save()
    # try:
    #     q.save()
    # except IntegrityError:
    #     q.is_mc = not q.is_mc
    #     q.save()
