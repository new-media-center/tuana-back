"""
WSGI config for herzkompass project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
import sys
import time
import traceback
import signal
from django.core.wsgi import get_wsgi_application  # noqa: E402

os.environ["DJANGO_SETTINGS_MODULE"] = "config.settings.production"

try:
    application = get_wsgi_application()
except RuntimeError as re:
    print(re)
    print('handling WSGI exception')
    # Error loading applications
    if 'mod_wsgi' in sys.modules:
        traceback.print_exc()
        os.kill(os.getpid(), signal.SIGINT)
        time.sleep(2.5)
