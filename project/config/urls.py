# -*- coding: utf-8 -*-

from django.conf import settings

from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.urls import path

from django.views import defaults as default_views

from django.conf.urls.static import static
from django.views.decorators.csrf import csrf_exempt
from general.views import SendFeedbackView

from app.admin import admin_site

def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns = [
    path('admin', admin_site.urls),
    path('feedback/', csrf_exempt(SendFeedbackView.as_view())),
    path('', include('app.urls')),
    path('sentry-debug/', trigger_error),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)



urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    import debug_toolbar
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
