import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
BASE_DIR = os.path.join(BASE_DIR, '../')
SECRET_KEY = 'z17*+9#gpdd#_kssvfw=i*v4^(8#7p#nwgp)thew)$2u9+dv)c'
DEBUG = False

DEFAULT_FROM_EMAIL = 'notifications-nmc@unibas.ch'
EMAIL_HOST = "smtp.unibas.ch"
EMAIL_PORT = 25
EMAIL_HOST_USER = 'notifications-nmc@unibas.ch'

SITE_ID = 1

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'rest_framework',
    'corsheaders',
    'app.apps.TuanaAdminConfig',
    'psycopg2',
]

INTERNAL_IPS = [
    '127.0.0.1',
]

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': True,
        },
    }
}
LOGIN_REDIRECT_URL = '/admin/'
LOCAL_APPS = [
    'app'
]
INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS
MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# credentials for test database
DATABASES = {
    'default': { # old db I found on my laptop
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tuana-db',
        'USER': 'tuana-dbuser',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': "5432",
    },
    # 'default': { # fresh db from appuio
    #     'ENGINE': 'django.db.backends.postgresql',
    #     'NAME': 'tuana-db-fresh',
    #     'USER': 'tuana-dbuser',
    #     'PASSWORD': '',
    #     'HOST': 'localhost',
    #     'PORT': "5432",
    # },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',  # NOQA
    'PAGE_SIZE': 10
}
LANGUAGE_CODE = 'de'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
ADMINS = (
    ("""admin""", 'notifications-nmc@unibas.ch'),
)
