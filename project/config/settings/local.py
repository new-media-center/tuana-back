# -*- coding: utf-8 -*-
from .base import *  # noqa: F403

ALLOWED_HOSTS = [
    # '.example.com',  # Allow domain and subdomains
    # '.example.com.',  # Also allow FQDN and subdomains
    '*',
]
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOWED_ORIGINS = [
    'http://localhost:8080',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tuana-db',
        'USER': 'tuana-dbuser',
        'PASSWORD': '',
        'HOST': '/tmp/pghost',
        'PORT': "5432"
    }
}

LOCAL_APPS = [
    'debug_toolbar',
    'django_extensions',
    'django.contrib.staticfiles',
]

MIDDLEWARE.extend([  # noqa: F405
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware'
])

INSTALLED_APPS = INSTALLED_APPS + LOCAL_APPS  # noqa: F405

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# when serving static files, django searches this folder:
# ADDITIONALLY to STATIC_ROOT, so not needed in our case
# STATICFILES_DIRS = [os.path.join(BASE_DIR, "../static")]
# django collectstatic will put the files into this folder:
STATIC_ROOT = os.path.join(BASE_DIR, "../collected_static")
