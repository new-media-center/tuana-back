# -*- coding: utf-8 -*-
from .base import *  # noqa: F403,F401

# if DEBUG is set to false, manage.py runserver doesnt deliver static files!
DEBUG = True

ALLOWED_HOSTS = [
    # '.example.com',  # Allow domain and subdomains
    # '.example.com.',  # Also allow FQDN and subdomains
    '.unibas.ch',
]
