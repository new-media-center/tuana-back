from .base import *  # noqa: F403,F401
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

ALLOWED_HOSTS = [
     os.environ['DJANGO_ALLOWED_HOST']
]

CORS_ALLOWED_ORIGINS = [
    'https://tutorial.anatomie.unibas.ch',
    'https://nmc-tuana-24.nmc.unibas.ch',
    'https://nmc-tuana-back-24.nmc.unibas.ch'
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

MEDIA_ROOT = os.path.join("/opt/media")
SECRET_KEY = os.environ['DJANGO_SECRET']

DATABASES = {
    'default': { # old db I found on my laptop
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['DATABASE_NAME'],
        'USER': os.environ['DATABASE_USER'],
        'PASSWORD': os.environ['DATABASE_PASSWORD'],
        'HOST': os.environ['DATABASE_HOST'],
        'PORT': "5432",
    },
}


sentry_sdk.init(
    dsn="https://b6325839f00c4b9da2413e90acb83e9d@o398207.ingest.sentry.io/5286693",
    integrations=[DjangoIntegration()],

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)
