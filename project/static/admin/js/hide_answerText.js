django.jQuery(document).ready(function() {
element_value = django.jQuery('#id_element').val();
  if (element_value == '') {
    django.jQuery("#id_answer_text").show();
  } else {
    django.jQuery("#id_answer_text").hide();
  }
  django.jQuery(document.body).on("change","#id_element",function() {
    element_value = django.jQuery('#id_element').val();
    if (element_value > 0) {
      django.jQuery("#id_answer_text").hide();
    } else {
      django.jQuery("#id_answer_text").show();
    }
  });
});
