from django.conf import settings
from django.db import connection
from app.models import Answer, ElementMedia
import time

import logging
logger = logging.getLogger(__name__)

# presort many2many table data to gain speed
with connection.cursor() as cursor:
    cursor.execute(
        """
        SELECT app_media_structures.media_id, app_media_structures.structure_id, app_structure.name
        FROM app_media_structures
        INNER JOIN app_structure
        ON app_media_structures.structure_id = app_structure.id;
        ;
        """
        )
    mediaStructures = cursor.fetchall()

    cursor.execute(
        """
        SELECT app_element_structures.element_id, app_element_structures.structure_id, app_structure.name
        FROM app_element_structures
        INNER JOIN app_structure
        ON app_element_structures.structure_id = app_structure.id;
        ;
        """
        )
    elementStructures = cursor.fetchall()

    cursor.execute(
        """
        SELECT app_elementmedia.element_id, app_media.id, app_media.name, app_media.url, app_media.beschreibung
        FROM app_elementmedia
        INNER JOIN app_media
        ON app_elementmedia.media_id = app_media.id;
        """
        )
    elemMedia = cursor.fetchall()

    cursor.execute(
        """
        SELECT app_element_topics.element_id, app_element_topics.topic_id, app_topic.name, app_topic.nick
        FROM app_element_topics
        INNER JOIN app_topic
        ON app_element_topics.topic_id = app_topic.id;
        ;
        """
        )
    elementTopics = cursor.fetchall()

    cursor.execute(
        """
        SELECT app_answer.id,
               app_answer.text,
               app_questionanswer.question_id,
               app_questionanswer.correct AS correct,
               app_element.name_med,
               app_element.name_alt
        FROM app_answer
        INNER JOIN app_questionanswer
        ON app_answer.id = app_questionanswer.answer_id
        LEFT JOIN app_element
        ON app_element.id = app_answer.element_id
        """
    )
    qanswer = cursor.fetchall()

def getRandomElementMediaByElementId(eId):
    return ElementMedia.objects.raw(
        """
        SELECT *, app_media.url AS media_url
        FROM app_elementmedia
        INNER JOIN app_media
        ON app_elementmedia.element_id = {0} AND app_elementmedia.media_id = app_media.id
        ORDER BY random() LIMIT 1
        """.format(eId)
        )[0]

def answer(item):
    return {
        'answer_id': item.answer_id,
        'answer_text': item.answer_text,
        'correct': item.correct,
        'question': item.question_id,
        'element': item.element_id
    }

def element(e):
    topic_list = [] # list of dicts, duplicates of dicts cannot be found with set()
    media_list = [] # so we apply a more complicated approach than for lists of primitives:
    for (eId, tId, tName, tNick) in elementTopics:
        if eId == e.pk:
            if not any(map(lambda t: t['id'] == tId, topic_list)): # prevent duplicates
                topic_list.append({
                    'id': tId,
                    'name': tName,
                    'nick': tNick
                })

    for (eId, mId, mName, mUrl, mBes) in elemMedia:
        if eId == e.pk:
            if not any(map(lambda m: m['id'] == mId, media_list)):
                media_list.append({
                    'id': mId,
                    'name': mName,
                    'url': mUrl,
                    'beschreibung': mBes
                })
    return {
        'id': e.id,
        'name': e.name_med,
        'name_med': e.name_med if e.name_med else '', # null values are hard to handle in frontend
        'name_alt': e.name_alt if e.name_alt else '',
        'structure_list': set(
            structureName for elementId, structureId, structureName in
            elementStructures if elementId == e.pk
        ),
        'topic_list': topic_list,
        'media_list': media_list
    }

def media(m):
    # demonstration of slow vs fast dict construction
    # t2 = time.process_time()
    # typ = m.typ.name
    # t211 = time.process_time()
    # print('{} serialize assigned typ through model'.format(t2-t211))
    typ = m.typ_id
    # t2111 = time.process_time()
    # print('{} serialize assigned typ sql query'.format(t211-t2111))
    return {
        'id': m.id,
        'url': "{0}{1}".format(settings.MEDIA_URL, str(m.url)),
        'name': m.name,
        'beschreibung': m.beschreibung,
        'structure_list': set(
            structureName for mediaId, structureId, structureName in mediaStructures if mediaId == m.pk
        ),
        'typ': [m.typ_name],
    }

def elementMedia(item):
    return {
        'id': item.id,
        'element': {
            'name_med': item.element_name,
            'element_id': item.element_id
        },
        'is_overview': item.is_overview,
        'media': item.media_id,
        'no': item.no,
        'position': item.position,
        'position_text': item.position_text
    }


def question(item):
    return {
        'question_id': item.id,
        'question_text': item.text,
        'typ': 1 if item.is_mc else 0,
        'sequenz': 0 if item.is_primary else 1,
        'modus': item.modus,
        'correct_answers_needed': item.correct_answers_needed,
        'published': item.published,
        'element': item.element_id
    }

def questionForTest(item, appendix = ""):
    return {
        'id': getattr(item, 'id' + appendix),
        'topic_id': getattr(item, 'topic_id' + appendix),
        'question_text': getattr(item, 'text' + appendix),
        'modus': 'Learn' if getattr(item, 'modus' + appendix) == 0 else 'Exam',
        'sequenz': 'Primary' if getattr(item, 'is_primary' + appendix) else 'Secondary',
        'typ': 'Multiple choice' if getattr(item, 'is_mc' + appendix) else 'Text',
        'correct_answers_needed': getattr(item, 'correct_answers_needed' + appendix),
        'answers': [{
            'answer_id': ansId,
            'answer_text': ansText,
            'element': nameMed,
            'element_alt': nameAlt,
            'correct': ansCorrect,
        } for (ansId, ansText, questionId, ansCorrect, nameMed, nameAlt) in qanswer if questionId == getattr(item, 'id' + appendix)]
    }

def testQuestion(q):
    fstQuestion = questionForTest(q)
    fstQuestion['element'] = getattr(q, 'element_name')
    fstQuestion['element_id'] = getattr(q, 'element_id')
    sndQuestion = questionForTest(q, "_sec")
    sndQuestion['element'] = getattr(q, 'element_name')
    sndQuestion['element_id'] = getattr(q, 'element_id')
    fstQuestion['secondary_questions'] = [sndQuestion]
    elementMedia = getRandomElementMediaByElementId(q.element_id)
    fstQuestion['media'] = [{
        'url': "{0}{1}".format(settings.MEDIA_URL, str(elementMedia.media_url)),
        'pin_position': elementMedia.position,
        'pin_position_text': elementMedia.position_text,
    }]
    return fstQuestion

def structure(item):
    return {
        'pk': item.id,
        'name': item.name
        }
