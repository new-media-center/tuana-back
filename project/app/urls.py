from django.conf import settings
from django.conf.urls import include
from django.db import connection
from django.urls import path

# create or replace view for bewegung/topo/neuro
# should be executed once at startup
# must be done before importing views
# ! if you run into problems while migrating: django migrate must be run with --skip-checks flag
from app.models import Topic
with connection.cursor() as cursor:
    cursor.execute(
        """
        CREATE OR REPLACE VIEW element_withmedia as
        WITH element_media_full AS (
            SELECT app_element.id,
                   app_element.name_med,
                   app_element.name_alt,
                   ROW_NUMBER () OVER (PARTITION BY app_element.id)
            FROM app_element
            INNER join app_elementmedia
            ON app_element.id = app_elementmedia.element_id
        )
        SELECT * FROM element_media_full
        WHERE row_number = 1
        """
    )
    for t in Topic.objects.all():
        cursor.execute(
            """
            CREATE OR REPLACE VIEW element_{0} as
            SELECT app_element.id, app_element.name_med, app_element.name_alt
            FROM app_element
            INNER JOIN app_element_topics
            ON app_element.id = app_element_topics.element_id
            INNER JOIN app_topic
            ON app_element_topics.topic_id = app_topic.id
            WHERE app_topic.nick = '{0}';
            """.format(t.nick)
            )
        # I found elements of published questions that do not have a media assigned.
        # That makes the frontend confused. Here we filter out elements without media:
        cursor.execute(
            """
            CREATE OR REPLACE VIEW element_{0}_withmedia as
            WITH element_media_full AS (
                SELECT element_{0}.id, element_{0}.name_med, element_{0}.name_alt, ROW_NUMBER () OVER (
                PARTITION BY element_{0}.id
                )
                FROM element_{0}
                INNER join app_elementmedia
                ON element_{0}.id = app_elementmedia.element_id
            )
            SELECT * FROM element_media_full
            WHERE row_number = 1
            """.format(t.nick)
            )
    # some elements have a primary but not a secondary question. Here, we create a view
    # that filters those out.
    cursor.execute(
        """
        CREATE OR REPLACE VIEW prim_sec_q as
        WITH q_prim AS (
            SELECT * FROM app_question WHERE app_question.published AND app_question.is_mc = 'f' AND app_question.is_primary
        ),   q_sec AS (
            SELECT
            element_id             as element_id_sec,
            id                     as id_sec,
            text                   as text_sec,
            is_mc                  as is_mc_sec,
            is_primary             as is_primary_sec,
            correct_answers_needed as correct_answers_needed_sec,
            modus                  as modus_sec,
            published              as published_sec,
            topic_id               as topic_id_sec
            FROM app_question WHERE app_question.published AND NOT app_question.is_primary
        )
        SELECT * FROM q_prim
        INNER JOIN q_sec
        ON q_prim.element_id = q_sec.element_id_sec AND q_prim.topic_id = q_sec.topic_id_sec
        """)
    # create mediaStructures view
    cursor.execute(
        """
        CREATE OR REPLACE VIEW app_media_structures AS
        SELECT app_elementMedia.media_id, app_element_structures.structure_id
        FROM app_elementmedia
        INNER JOIN app_element_structures
        ON app_elementMedia.element_id = app_element_structures.element_id
        """
    )
    cursor.execute(
        """
        CREATE OR REPLACE VIEW app_question_answers AS
        SELECT app_answer.id AS answer_id, app_answer.text AS answer_text, app_questionanswer.correct, app_questionanswer.question_id AS question
        FROM app_answer
        INNER JOIN app_questionanswer
        ON app_answer.id = app_questionanswer.answer_id
        """
    )

from app import views

app_name = 'tuana'

urlsubpatterns = [
    path('answer/', views.ListAnswers.as_view()),
    path('elementmedia/', views.ListElementMedia.as_view()),
    path('element/', views.ListElements.as_view()),
    path('media/', views.ListMedia.as_view()),
    path('question/', views.ListQuestions.as_view()),
    path('structure/', views.ListStructures.as_view()),
    path('test/', views.TestSet.as_view()),
]

urlpatterns = [
    # I)  we dont need to get all questions of all topics
    # II) we have to generate views for all topics to make this work
    # path('',
    #      include(urlsubpatterns), { 'topic': 'app_element' }),
    path('bewegungsapparat/',
         include(urlsubpatterns), { 'topic': 'bewegung' }),
    path('neuro/',
         include(urlsubpatterns), { 'topic': 'neuro' }),
    path('topographische/',
         include(urlsubpatterns), { 'topic': 'topo' })
    # path(r'test/', get_testSet),
    ]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
