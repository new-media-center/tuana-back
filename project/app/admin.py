# -*- coding: utf-8 -*-
import csv
from django.contrib import admin
from django.utils.html import format_html
from django.shortcuts import redirect
from django.http import HttpResponse
from django.db import connection

from .models import (Topic, Structure, Media, Answer, Element, ElementMedia,
                     Question)

class TuanaAdminSite(admin.AdminSite):
    enable_nav_sidebar = False

# https://books.agiliq.com/projects/django-admin-cookbook/en/latest/export.html
class ExportCsvMixin:
    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)
        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = "Export Selected"

# Filter plugins for Topic filters
class TopicMediaFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = ('Topic filter')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'topic'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('bewegung', ('Bewegungsapparat')),
            ('neuro', ('Neuroanatomie')),
            ('topo', ('Topographische')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if (self.value()):
            raw_query = '''
                SELECT app_elementmedia.media_id
                FROM app_element_{0}
                INNER JOIN app_elementmedia
                ON app_element_{0}.id = app_elementmedia.element_id
            '''.format(self.value())
            cursor = connection.cursor()
            try:
                cursor.execute(raw_query)
                mediaIdList = set([x[0] for x in cursor])
                # We have to return a queryset, not a rawQuerySet or anything else
                # We construct the queryset with this hack:
                queryset = queryset.filter(id__in=mediaIdList)
            finally:
                cursor.close()
        return queryset


class TopicQuestionFilter(admin.SimpleListFilter):
    title = ('Topic filter')
    parameter_name = 'topic'
    def lookups(self, request, model_admin):
        return (
            ('bewegung', ('Bewegungsapparat')),
            ('neuro', ('Neuroanatomie')),
            ('topo', ('Topographische')),
        )

    def queryset(self, request, queryset):
        if (self.value()):
            q=queryset.filter(topic__nick=self.value())
        else:
            q = queryset
        return q

class QuestionInline(admin.TabularInline):
    model = Question
    extra = 0

class ElementAdmin(admin.ModelAdmin, ExportCsvMixin):
    # list
    search_fields = ('name_med', 'name_alt')
    list_display = (
        'pk',
        'name_med',
        'name_alt',
        'get_questions',
        'media_count',
    )
    list_filter = ("topics",)
    actions = ["export_as_csv"]
    extra = 0
    def get_questions(self, obj):
        return [q.text for q in obj.questions.all()]
    def media_count(self, obj):
        return obj.media.count()
    # change
    filter_horizontal = ['structures', 'media']
    inlines = [ QuestionInline, ]

class MediaAdmin(admin.ModelAdmin, ExportCsvMixin):
    class Media:
        js = ('admin/js/vendor/jquery/jquery.min.js',
              'admin/js/jquery.init.js',
              'admin/js/raphael.min.js',
              'admin/js/wz_jsgraphics.js',
              'admin/js/tutorial-anatomie.js',
              )
    list_display = (
        'id',
        'image_tag',
        'url',
        'name',
        'typ',
        'beschreibung',
    )
    fields = ['name', 'url', 'typ', 'beschreibung', ]
    list_filter = (TopicMediaFilter,)
    search_fields = ('name', )
    actions = ["export_as_csv"]
    def image_tag(self,obj):
        return format_html('<img src="/media/{0}" style="height:45px;" />'.format(obj.url))

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['object'] = self.get_object(request, object_id)
        return super(MediaAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

class StructureAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'name',
    )

class AnswerAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = (
        'pk',
        'element',
        'text',
    )
    autocomplete_fields = ('element',)
    search_fields = ('text', 'element__name_med', 'pk',)
    actions = ["export_as_csv"]

class AnswerInline(admin.TabularInline):
    model = Question.answers.through
    autocomplete_fields = ('answer',)
    extra = 0


class QuestionAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = (
        'pk',
        'element_name',
        'text',
        'is_mc_display',
        'is_primary',
        'modus_display',
        'correct_answers_needed',
        'published',
        'topic',
    )
    def element_name(self, obj):
        return obj.element.name_med

    def is_mc_display(self, obj):
        if (obj.is_mc):
            return 'multiple choice'
        return 'text'

    def modus_display(self, obj):
        if (obj.modus == 0):
            return 'learn'
        return 'exam'

    is_mc_display.short_description = 'mc/text'
    is_mc_display.admin_order_field = 'is_mc'
    modus_display.short_description = 'modus'
    modus_display.admin_order_field = 'modus'
    element_name.admin_order_field = 'element__name_med'
    list_filter = (TopicQuestionFilter,)
    actions = ["export_as_csv"]
    autocomplete_fields = ('element',)
    extra = 0
    search_fields = ('text', 'pk', 'element__name_med')
    actions = ["export_as_csv"]
    inlines = [ AnswerInline, ]

class ElementMediaAdmin(admin.ModelAdmin):
    class Media:
        js = ('admin/js/vendor/jquery/jquery.min.js',
              'admin/js/jquery.init.js',
              'admin/js/raphael.min.js',
              'admin/js/wz_jsgraphics.js',
              'admin/js/tutorial-anatomie.js',
              '/static/admin/js/hide_position.js',
              )

    add_form_template = 'admin/app/elementmedia/add_form.html'
    model = ElementMedia
    exclude = ('media', )

    # Return empty perms dict thus hiding the model from admin index.
    def get_model_perms(self, request):
        return {}

    def add_view(self, request, form_url='', extra_context=None):
        # get id of media by splitting the url
        media_pk = request.GET.get('media_pk')
        extra_context = extra_context or {}
        extra_context = {'title': 'Pin für Element setzen'}
        extra_context['media_file'] = Media.objects.get(pk=media_pk)
        return super(ElementMediaAdmin, self).add_view(
            request, form_url, extra_context=extra_context,)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context = {'title': 'Pin für Element setzen'}
        extra_context['media_file'] = ElementMedia.objects.get(
            pk=object_id).media
        extra_context['element_media_object'] = ElementMedia.objects.get(
            pk=object_id)
        return super(ElementMediaAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context, )

    def response_add(self, request, obj, post_url_continue=None):
        media_pk = (request.get_raw_uri()).split('=', 1)[-1]
        return redirect('tuanaAdmin:app_media_change',
                        object_id=media_pk)

    def response_change(self, request, obj):
        self.changed_fk = ElementMedia.objects.get(id=obj.pk).media.pk
        return redirect('tuanaAdmin:app_media_change',
                        object_id=self.changed_fk)

    # creating deleted_fk variable to store parent of deleted element
    def delete_view(self, request, object_id, extra_context=None):
        self.deleted_fk = ElementMedia.objects.get(id=object_id).media.pk
        return super(ElementMediaAdmin, self).delete_view(request, object_id,
                                                          extra_context)

    # using deleted_fk from delete_view to redirect to parent media element
    def response_delete(self, request, obj_display, obj_id):
        return redirect('tuanaAdmin:app_media_change',
                        object_id=self.deleted_fk)

    def save_model(self, request, obj, form, change):
        if not obj.media_id:
            obj.media_id = request.get_raw_uri().split('=', 1)[-1]
        obj.save()

admin_site = TuanaAdminSite(name='tuanaAdmin')
admin_site.register(Media, MediaAdmin)
admin_site.register(Element, ElementAdmin)
admin_site.register(ElementMedia, ElementMediaAdmin)
admin_site.register(Structure, StructureAdmin)
admin_site.register(Answer, AnswerAdmin)
admin_site.register(Question, QuestionAdmin)
