{
  description = "Nmc Tuana flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  };
  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [];
    };
  in rec {
    defaultPackage.x86_64-linux = devShell.x86_64-linux;
    devShell.x86_64-linux = pkgs.mkShell {
      # buildInputs is for dependencies you'd need "at run time",
      # were you to to use nix-build not nix-shell and build whatever you were working on
      buildInputs = with pkgs; [
        postgresql
        python3
        python3Packages.pip
      ];
      shellHook = ''
        export PS1='\u@tuana-back \$ '
        export PIP_PREFIX=$(pwd)/_build/pip_packages
        export PYTHONPATH="$PIP_PREFIX/${pkgs.python3.sitePackages}:$PYTHONPATH"
        export PATH="$PIP_PREFIX/bin:$PATH"
        unset SOURCE_DATE_EPOCH
        export PGDATA=pgdata
        export LOG_PATH=pglog
        export PGHOST=/tmp/pghost
        export PGDATABASE=tuana-db
        export DATABASE_URL="postgresql:///$PGDATABASE?host=$PGHOST"
        if [ ! -d $PGHOST ]; then
          mkdir -p $PGHOST
        fi
        if [ ! -d $PGDATA ]; then
          echo 'Initializing postgresql database...'
          initdb $PGDATA -U tuana-dbuser  --auth=trust >/dev/null
        fi
        echo "Checking postgres status..."
        pg_ctl status
        if [ $? -eq 3 ]; then
          pg_ctl start -l $LOG_PATH -o "-c listen_addresses= -c unix_socket_directories=$PGHOST"; return;
        fi
      '';
    };
  };
}
