#!/bin/bash

npm run build:css
npm run build:js
source $WORKON_HOME/tutorial-anatomie-django/bin/activate \
	&& python project/manage.py collectstatic --noinput

# shell.exec("npm run build:locale"); # Don't build each time; It's sensitive
