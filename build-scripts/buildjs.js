#! /usr/bin/env node
/*jshint esversion: 6 */
/*
* Add the steps to take to create deployable js files. I.e. minify, uglify,
* union, copy, ...
* As an example, you'll find the steps we took for the eEducation project
*/

const sh = require('shelljs');

const pathBase = 'project/app/src/js/';

const pathStatic = 'project/static/';

if (!sh.test('-e', pathBase)) {
  sh.mkdir('-p', pathBase);
}

sh.cat(['project/app/src/js/*.*']).to(`${pathStatic}js/tutorial-anatomie.js`);
