import anatomie_bewegungsapparat.models as bModel
import anatomie_topographische.models as tModel
import anatomie_neuro.models as nModel
import json

dbName = 'mysql_fresh'
# dbName = 'mysql'
topicList = [{
        'name': 'Bewegungsapparat',
        'nick': 'bewegung',
        'model': bModel
    }, {
        'name': 'topographische Anatomie',
        'nick': 'topo',
        'model': tModel
    }, {
        'name': 'neurologische Anatomie',
        'nick': 'neuro',
        'model': nModel
    }]

newL = [{ 'name': t['name'], 'nick': t['nick'] } for t in topicList]

eList = []
# list of orphan questions (not connected to an element)
qList = []

for m in topicList:
         print(m['nick'])
         for e in m['model'].Element.objects.using(dbName).all():
                  val = {
                      'name_alt': None if e.alternative_schreibweise == '' else e.alternative_schreibweise,
                      'name_med': None if e.name == '' else e.name,
                      'topic': {
                          'name': None if m['name'] == '' else  m['name'],
                          'nick': None if m['nick'] == '' else  m['nick']
                          },
                      'structures': [{ 'name': s.name } for s in e.structures.all()],
                      'elementMedia': [{
                                          'media': {
                                                       'typ': elemMed.media.typ,
                                                       'url': str(elemMed.media.url),
                                                       'name': elemMed.media.name,
                                                       'beschreibung': None if elemMed.media.beschreibung == '' else elemMed.media.beschreibung
                                                   },
                                          'position': None if elemMed.position == '' else elemMed.position,
                                          'position_text': None if elemMed.position_text == '' else elemMed.position_text,
                                          'no': None if elemMed.no == '' else elemMed.no,
                                          'is_overview': elemMed.is_overview
                                      } for elemMed in e.elementmedia_set.all()],
                      'questions': [{
                                       'typ': q.typ,
                                       'sequenz': q.sequenz,
                                       'modus': q.modus,
                                       'correct_answers_needed': q.correct_answers_needed,
                                       'published': q.published,
                                       'text': q.question_text,
                                       'answers': [{
                                                      'text': str(a.element) if a.answer_text == '' else a.answer_text,
                                                      'correct': a.correct
                                                  } for a in q.answers.all()]
                                   } for q in e.questions.all()]
                  }
                  eList.append(val)

         # Not to forget general questions (not bound to any element)
         for q in m['model'].Question.objects.using(dbName).filter(element=None):
                  qList.append({
                           'typ': q.typ,
                           'topic': m['nick'],
                           'sequenz': q.sequenz,
                           'modus': q.modus,
                           'correct_answers_needed': q.correct_answers_needed,
                           'published': q.published,
                           'text': q.question_text,
                           'answers': [{
                                    'text': str(a.element) if a.answer_text == '' else a.answer_text,
                                    'correct': a.correct
                           } for a in q.answers.all()]
                  })





with open("db_dumps/tuanaQuestionsDB.json", "w") as outfile:
    json.dump(qList, outfile)

with open("db_dumps/tuanaQuestionsDBpretty.json", "w") as outfile:
    json.dump(qList, outfile, indent=2)

with open("tuanaDB.json", "w") as outfile:
    json.dump(eList, outfile)

with open("tuanaDBpretty.json", "w") as outfile:
    json.dump(eList, outfile, indent=2)
