# Online Tutorial Anatomie 
## Frontend Übersicht

### Wählbare Modi
#### Anschauen (json: /anschauen/\<strukturfilter\>/\<Medientypfilter\>)
###### Wählbare Filter: 
- Struktur, (Brust, Kopf, Beckengürtel ...) (Punkt 'Wählen Sie einen Bereich aus.' im bestehendem Tutorial)  
Kann per url gefiltert werden
- Medientyp (Foto, Schnitt, Radiologie) (Punkt 'Mit welchen Medientypen wollen Sie lernen?' im bestehendem Tutorial)  
Wird sinnvollerweise auch bereits vor json Lieferung gefiltert (noch nicht implementiert, wird aber gemacht - 28.8.2018)
###### Ansicht:
- Breadcrumby Überschrift (Struktur - Element - Beschreibung (media.beschreibung) - Abhängig von angewähltem Element. *Nachfragen, ob angezeigt werden soll.*
- Bild (url)
- Elemente (elementmedia)
  - Pins (no, position, position_text, is_overview)
  - Elementnamen (element)
  - Elementnummern (no)  

*Anzeige elementmedia.alternative_schreibweise im Frontend ungeklärt. Nachfragen.
Wird evtl nur als weitere korrekte Antwortmöglichkeit bei Freitexteingabe verwendet.*

#### Memorieren (json: /anschauen/\<strukturfilter\>/\<Medientypfilter\>)
###### Wählbare Filter: 
Gleich wie 'Anschauen'
###### Ansicht:
Gleich wie 'Anschauen', **ausser**: Die Elementnummern werden ohne Elementnamen angezeigt. Elementnamen werden erst angezeigt, wenn sie 'angefragt' werden (z.B Klick auf Elementnummer o.ä.). 

![json anschauen](json_anschauen.png)

#### Üben / Test (json: /test/\<strukturfilter\>/\<Medientypfilter\>)
###### (Vormals 'Multiple Choice' und 'Freitexteingabe'. Soll neu nicht mehr wählbar sein. Multiple Choice und Freitexteingabefragen sollen gemischt erscheinen, wie bei vormaligem 'Test')
###### Wählbare Filter: 
- Struktur
- Medientyp 
###### Ansicht:
- Bild (media.url)
- Element (element)
  - Pin (media.pin_no, media.pin_position, media.pin_position_text)
- Question (question_text)
- Answers
  - Multiple Choice
    - Antwortmöglichkeiten (answer[n].element ODER answer[n].answer_text)
  - Freitext
    - Eingabefeld

Zeitvorgabe. Dauer nachfragen?

#### Prüfungssimulation (json: /test/\<strukturfilter\>/\<Medientypfilter\>)
Wie Üben / Test aber: Keine Filter zur Vorauswahl.

![json test](json_test.png)
