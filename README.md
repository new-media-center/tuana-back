## Installation

``` bash 
pip install -r requirements/local.txt
```

## Transfer old mysql db to new postgresql db                                                                                                                                 
                                                                                                                                                                              
In 2020 we renovated the this project and switched from a mysql to postgresql.
The old db can be converted with the help of two scripts:

- ./oldDB2json.py
- ./json2newMySQL.py

Steps needed to convert. First get a dump of the production mysql database.
Import it to the tuana project. Run oldDB2json.py on the project to get a json,
database independent, representation of the data.

1. Get a dump from the current production live system.

We open a remote shell session in the production server (appuio at the moment).
The openshift db is named tuana-mysql. From there we dump the production database:

 ``` bash
 mysqldump -u tuana_db_user -p tuana_db > 20201130-appuio_tuana-mysql.sql
 ```

2. Import production database to a local django project

Switch to branch lastChanceToDumbOldMysqlDB. You need a fresh installation of
mysql and mysql-client, mysql version 5.x, eg 5.7.

``` bash
# initialize-insecure -> empty root password
mysqld --initialize-insecure --datadir=./mysql-data --socket=/tmp/mysql.socket
```

Create a new database tuana_db and its user.

``` mysql 
MysqlDB [(none)]> CREATE DATABASE tuana_db;
MysqlDB [(none)]> CREATE USER 'tuana_db_user'@'localhost' IDENTIFIED BY 'test_pass';
MysqlDB [(none)]> GRANT ALL PRIVILEGES ON tuana_db.* TO 'tuana_db_user'@'localhost';
MysqlDB [(none)]> FLUSH PRIVILEGES;
MysqlDB [(none)]> quit
```

Import database and migrate other apps

``` bash
mysql -u tuana_db_user -p tuana_db < data-dump.sql
python project/manage.py migrate --fake-initial
```

Get json representation of current database.

``` bash
python project/manage.py shell < oldDB2json.py
```

3. Import json representation to posgresql:

Check out the current branch. 

 ``` bash
dropdb --h /private/nmc/tutorial-anatomie-django/postgres -U tuana-dbuser tuana-db
createdb --h /private/nmc/tutorial-anatomie-django/postgres -U tuana-dbuser tuana-db
# python project/manage.py migrate --fake-initial
python project/manage.py shell < json2newMySQL.py
 ```
 
