---
date: 20. November 2023
pagestyle: empty
header-includes: |
  \usepackage{fancyhdr}
  \pagestyle{fancy}
  \fancyhf{}
  \fancyhead[R]{20. November 2023}
...


# Aufwandschätzung Anpassungen Tutorial Anatomie

## Änderungen

### Reihenfolge der Medieninhalte nach Titel ordnen

Momentan werden im Anschauen- und Memorieren-Modus die Medien in willkürlicher
Abfolge angezeigt.

Die gleichen Abbildungen sind im Skript gemäss dem Aufbau der Vorlesung in
einer bestimmten Reihenfolge platziert. Herrn Kapfhammer ist es nun ein
grosses Anliegen, diese Reihenfolge auch im Tutorial abzubilden.

### Strukturfilter wird von Anatomie-Personal eigenhändig verwaltet

Die Struktur dient im Tutorial neben dem Medientyp zur logischen Unterteilung
von anatomischen Abbildungen. Herr Kapfhammer möchte für sein Team eine
Funktion, die es ermöglicht, Strukturen zu bearbeiten.


## Aufwandschätzung

Beide Verbesserungen können mit geringem Aufwand umgesetzt werden. Um die
Weiterarbeit am Tutorial zu ermöglichen, müssen zuerst allerdings die Bibliotheken
aktualisiert werden. Da am Tutorial schon länger keine Arbeiten mehr
durchgeführt wurden, kann dies einige Zeit in Anspruch nehmen.

Bezüglich der Struktur-Anpassung existiert im Backend bereits die
gewünschte Funktionalität. Lediglich eine Instruktion des Anatomie-Teams ist
erforderlich.

-----------------  ----------
Aktualisierung:    8 Stunden
Reihenfolge:       2 Stunden
Struktur:          1 Stunde
-----------------  ----------
